package ServiceTags

type ServiceTags struct {
	ChangeNumber int      `json:"changeNumber"`
	Cloud        string   `json:"cloud"`
	Values       []Values `json:"values"`
}

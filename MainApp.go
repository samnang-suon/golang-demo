package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"text/template"
)

func main() {
	/*
		download "Azure IP Ranges and Service Tags – Public Cloud"
	*/
	fileUrl := "https://download.microsoft.com/download/7/1/D/71D86715-5596-4529-9B13-DA13A5DE5B63/ServiceTags_Public_20210913.json"
	err := DownloadFile("ServiceTags_Public_20210913.json", fileUrl)
	if err != nil {
		panic(err)
	}
	fmt.Println("Downloaded: " + fileUrl)

	/*
		Unmarshalling our JSON
	*/
	// Open our jsonFile
	jsonFile, err := os.Open("ServiceTags_Public_20210913.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened 'ServiceTags_Public_20210913.json'")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	// read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)
	// we initialize our ServiceTags array
	var serviceTags ServiceTags
	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'serviceTags' which we defined above
	json.Unmarshal(byteValue, &serviceTags)
	// we iterate through every user within our users array and
	// print out the user Type, their name, and their facebook url
	// as just an example
	fmt.Println("ServiceTags.ChangeNumber: " + string(serviceTags.ChangeNumber))
	fmt.Println("ServiceTags.Cloud: " + string(serviceTags.Cloud))
	for i := 0; i < len(serviceTags.Values); i++ {
		fmt.Println("ServiceTags.ServiceTag[i].Name: " + serviceTags.Values[i].Name)
		fmt.Println("ServiceTags.Values[i].Id: " + serviceTags.Values[i].Id)
		fmt.Println("ServiceTags.Values[i].Properties.ChangeNumber: " + string(serviceTags.Values[i].Properties.ChangeNumber))
		fmt.Println("ServiceTags.Values[i].Properties.Region: " + serviceTags.Values[i].Properties.Region)
		fmt.Println("ServiceTags.Values[i].Properties.RegionId: " + string(serviceTags.Values[i].Properties.RegionId))
		fmt.Println("ServiceTags.Values[i].Properties.Platform: " + serviceTags.Values[i].Properties.Platform)
		fmt.Println("ServiceTags.Values[i].Properties.SystemService: " + serviceTags.Values[i].Properties.SystemService)
		for k := 0; k < len(serviceTags.Values[i].Properties.AddressPrefixes); k++ {
			fmt.Println("ServiceTags.Values[i].Properties.AddressPrefixes[k]: " + serviceTags.Values[i].Properties.AddressPrefixes[k])
		}
		for k := 0; k < len(serviceTags.Values[i].Properties.NetworkFeatures); k++ {
			fmt.Println("ServiceTags.Values[i].Properties.NetworkFeatures[k]: " + serviceTags.Values[i].Properties.NetworkFeatures[k])
		}
	}

	/*
		Only keep 'AzureFrontDoor.Backend' ServiceTag
	*/
	var azureFrontDoorBackend Values
	for i := 0; i < len(serviceTags.Values); i++ {
		if serviceTags.Values[i].Name == "AzureFrontDoor.Backend" {
			fmt.Println("'AzureFrontDoor.Backend' was found.")
			azureFrontDoorBackend = serviceTags.Values[i]
			break
		}
	}
	fmt.Println("============================== 'AzureFrontDoor.Backend' IP List ==============================")
	for i := 0; i < len(azureFrontDoorBackend.Properties.AddressPrefixes); i++ {
		fmt.Println(azureFrontDoorBackend.Properties.AddressPrefixes[i])
	}

	/*
		Write 'azureFrontDoorBackend' json object to file
	*/
	writeToFile(azureFrontDoorBackend)

	/*
		Send email
	*/
	sendPlainTextEmail()
	sendHtmlEmail()
}

// DownloadFile will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
//
// solution from:
// https://golangcode.com/download-a-file-from-a-url/
func DownloadFile(filepath string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

// solution from:
// // https://www.golangprograms.com/golang-writing-struct-to-json-file.html
func writeToFile(p_AzureFrontDoorBackend Values) {
	file, _ := json.MarshalIndent(p_AzureFrontDoorBackend, "", " ")
	_ = ioutil.WriteFile("test.json", file, 0644)
}

// solution from:
// https://mailtrap.io/blog/golang-send-email/
func sendPlainTextEmail() {
	// Choose auth method and set it up
	auth := smtp.PlainAuth("", "samnang.suon@nuance.com", "extremely_secret_pass", "smtp.nuance.com")
	fmt.Println(auth)
	// Here we do it all: connect to our server, set up a message and send it
	to := []string{"samnang.suon@nuance.com"}
	msg := []byte("To: samnang.suon@nuance.com\r\n" +
		"Cc: samnang.suon@nuance.com\r\n" +
		"Subject: Golang Email Test\r\n" +
		"\r\n" +
		"<h1>Hello World !</h1>\r\n")
	// below 'nil' because of the following error message
	// 2021/09/16 20:07:20 smtp: server doesn't support AUTH
	var err = smtp.SendMail("smtp.nuance.com:25", nil, "samnang.suon@nuance.com", to, msg)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Plain Text Email Sent!")
}
func sendHtmlEmail() {
	// Sender data.
	from := "samnang.suon@nuance.com"
	password := "<Email Password>"

	// Receiver email address.
	to := []string{
		"samnang.suon@nuance.com",
	}

	// smtp server configuration.
	smtpHost := "smtp.nuance.com"
	smtpPort := "25"

	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpHost)
	fmt.Println(auth)

	t, _ := template.ParseFiles("template.html")

	var body bytes.Buffer

	mimeHeaders := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	body.Write([]byte(fmt.Sprintf(
		"To: samnang.suon@nuance.com\r\n"+
			"Cc: samnang.suon@nuance.com\r\n"+
			"Subject: This is a test subject \n%s\n\n", mimeHeaders)))

	t.Execute(&body, struct {
		Name    string
		Message string
	}{
		Name:    "Samnang Suon",
		Message: "This is a test message in a HTML template",
	})

	// Sending email.
	err := smtp.SendMail(smtpHost+":"+smtpPort, nil, from, to, body.Bytes())
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("HTML Email Sent!")
}

package Values

type Values struct {
	Name       string     `json:"name"`
	Id         string     `json:"id"`
	Properties Properties `json:"properties"`
}
